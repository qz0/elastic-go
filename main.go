package main

import (
	"elastic/log"
	"github.com/elastic/go-elasticsearch/v8"
)

func main() {

	es, _ := elasticsearch.NewDefaultClient()
	log.Lg.Info(elasticsearch.Version)
	log.Lg.Info(es.Info())
}
